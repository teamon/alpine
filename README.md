# Alpine runtime image

Alpine base image to run elixir releases

### Available versions
- `alpine:3.6`

### Updating
Edit `Dockerfile` and update `version` label. 
New release will be built after pushing to master.