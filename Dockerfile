FROM alpine:3.6
LABEL version="3.6"

# install dependencies
RUN apk add --no-cache --update bash openssl

# prepare app dir & user
RUN mkdir /app && chown -R nobody: /app
WORKDIR /app
USER nobody

# replace sys.config with ENV vars
ENV REPLACE_OS_VARS=true

# default command for elixir releases
CMD ["foreground"]
